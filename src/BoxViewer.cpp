// c++
#include <iostream>
#include <fstream>
#include <filesystem>
#include <string>
#include <string_view>
#include <cstdint>
#include <vector>
#include <numeric>
#include <unordered_map>

// ea
#include <include/console.hpp>

namespace eab
{
	using map_t = std::unordered_map <uint32_t, std::string>;

	struct body
	{
		uint32_t size;
		uint32_t offset;
	}; // struct body
} // namespace eam

namespace message
{
	int32_t example (void) noexcept
	{
		//
		ea::console::color::set (240);
		std::cout << "BoxViewer.exe box_path";

		ea::console::color::set (ea::console::color::value::DARKGRAY);
		std::cout << "\nshow entries\n\n";

		//
		ea::console::color::set (240);
		std::cout << "BoxViewer.exe box_path map_path";

		ea::console::color::set (ea::console::color::value::DARKGRAY);
		std::cout << "\nshow entries with custom map\n\n";

		return EXIT_SUCCESS;
	}

	template <typename Path>
	int32_t no_exist (Path const& _path) noexcept
	{
		std::cout << "file ";

		ea::console::color::set (240);
		std::cout << _path;

		ea::console::color::set (ea::console::color::value::DARKGRAY);
		std::cout << " ";

		ea::console::color::set (192);
		std::cout << "not found";

		ea::console::color::set (ea::console::color::value::DARKGRAY);
		return EXIT_FAILURE;
	} // int32_t no_exist (Path const& _path) noexcept

	template <typename Path>
	int32_t cant_open (Path const& _path) noexcept
	{
		std::cout << "file ";

		ea::console::color::set (240);
		std::cout << _path;

		ea::console::color::set (ea::console::color::value::DARKGRAY);
		std::cout << " ";

		ea::console::color::set (192);
		std::cout << "can't be opened";

		ea::console::color::set (ea::console::color::value::DARKGRAY);
		std::cout << '\n';
		return EXIT_FAILURE;
	} // int32_t cant_open (Path const& _path) noexcept

	template <typename Path>
	int32_t map_cant_open (Path const& _path) noexcept
	{
		std::cout << "map file ";

		ea::console::color::set (240);
		std::cout << _path;

		ea::console::color::set (ea::console::color::value::DARKGRAY);
		std::cout << " ";

		ea::console::color::set (192);
		std::cout << "can't be opened";

		ea::console::color::set (ea::console::color::value::DARKGRAY);
		std::cout << '\n';
		return EXIT_FAILURE;
	} // int32_t cant_open (Path const& _path) noexcept

	template <typename Path>
	int32_t map_no_exist (Path const& _path) noexcept
	{
		std::cout << "map file ";

		ea::console::color::set (240);
		std::cout << _path;

		ea::console::color::set (ea::console::color::value::DARKGRAY);
		std::cout << " ";

		ea::console::color::set (192);
		std::cout << "not found";

		ea::console::color::set (ea::console::color::value::DARKGRAY);
		std::cout << '\n';
		return EXIT_FAILURE;
	} // int32_t cant_open (Path const& _path) noexcept

	template <typename Path>
	void header (Path const& _path, uint32_t const _count) noexcept
	{
		std::cout << "file ";

		ea::console::color::set (240);
		std::cout << _path;

		ea::console::color::set (ea::console::color::value::DARKGRAY);
		std::cout << " contains ";

		ea::console::color::set (240);
		std::cout << _count;

		ea::console::color::set (ea::console::color::value::DARKGRAY);
		std::cout << " entries\n";
	} // void header (Path const& _path, eam::map::header const& _header) noexcept

	template <typename Path>
	void map_header (Path const& _path, uint32_t const _count) noexcept
	{
		std::cout << "map file ";

		ea::console::color::set (240);
		std::cout << _path;

		ea::console::color::set (ea::console::color::value::DARKGRAY);
		std::cout << " contains ";

		ea::console::color::set (240);
		std::cout << _count;

		ea::console::color::set (ea::console::color::value::DARKGRAY);
		std::cout << " entries\n";
	} // void header (Path const& _path, eam::map::header const& _header) noexcept

	template <typename Path>
	void body (
		Path const& _path,
		std::vector <eab::body> const& _body,
		eab::map_t const& _map
	) noexcept {
		const auto max_str_len = _map.empty () ? 0 : std::size ((*std::max_element (std::cbegin (_map), std::cend (_map), [](auto& _l, auto& _r) {
			return std::size (_l.second) < std::size (_r.second);
			})).second);

		for (eab::body const& body : _body) {
			ea::console::color::set (240);
			std::cout << "0x" << std::setfill ('0') << std::setw (std::numeric_limits <uint32_t>::digits10) << std::hex << std::right << body.offset;

			ea::console::color::set (ea::console::color::value::DARKGRAY);

			if (const auto& found{ _map.find (body.offset) }; std::cend (_map) != found) {
				std::cout << ' ' << std::setfill (' ') << std::setw (max_str_len) << std::left << found->second << ' ';
			} else { std::cout << ' ' << "no data" << ' '; }

			ea::console::color::set (240);
			std::cout << std::dec << body.size << '\n';
		}
	} // void body (Path const& _path, std::vector <eam::map::body> const& _body) noexcept
} // namespace message

template <typename MapPath>
eab::map_t read_map (MapPath const& _path) noexcept
{
	eab::map_t map;

	const std::filesystem::path map_path{ _path };
	if (!std::filesystem::exists (map_path)) {
		message::map_no_exist (map_path);
		return map;
	}

	std::ifstream map_file{ map_path, std::ifstream::binary };
	if (!map_file) {
		message::map_cant_open (map_path);
		return map;
	}

	uint32_t count{ 0 };
	map_file.read (reinterpret_cast <char*> (&count), sizeof (count));

	message::map_header (_path, count);

	for (uint32_t q{ 0 }; q != count; ++q) {
		uint32_t offset{ 0 };
		map_file.read (reinterpret_cast <char*> (&offset), sizeof (offset));

		uint8_t length{ 0 };
		map_file.read (reinterpret_cast <char*> (&length), sizeof (length));

		std::string path (static_cast <size_t> (length), '\0');
		map_file.read (std::data (path), length);

		map.emplace (offset, path);
	}
	return map;
}

std::vector <eab::body> read_box_body (uint32_t const _count, std::ifstream& _file) noexcept
{
	std::vector <eab::body> body;
	body.reserve (_count);

	for (uint32_t q{ 0 }; q != _count; ++q) {
		uint32_t offset{ static_cast <uint32_t> (_file.tellg ()) };

		uint32_t size{ 0 };
		_file.read (reinterpret_cast <char*> (&size), sizeof (size));

		body.emplace_back (eab::body{ size, offset });

		_file.seekg (static_cast <size_t> (offset) + static_cast <size_t> (size) + sizeof (size), std::ios::beg);
	}
	return body;
}

int32_t main (int32_t const argc, char const* argv[]) noexcept
{
	if (2 > argc) { return message::example (); }

	const std::filesystem::path path{ argv[1] };
	if (!std::filesystem::exists (path)) { return message::no_exist (path); }

	std::ifstream file{ path, std::ifstream::binary };
	if (!file) { return message::cant_open (path); }

	uint32_t count{ 0 };
	file.read (reinterpret_cast <char*> (&count), sizeof (count));
	message::header (path, count);

	const auto map{ read_map (2 == argc ? path.generic_string ().append (".map") : argv[2]) };
	const std::vector <eab::body> body{ read_box_body (count, file) };

	message::body (path, body, map);

	ea::console::color::set (ea::console::color::value::DARKGRAY);
	return EXIT_SUCCESS;
} // int32_t main (int32_t const argc, char const* argv[]) noexcept
